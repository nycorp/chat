package corp.ny.com.chat.env;

import android.content.SharedPreferences;

import corp.ny.com.chat.model.User;
import corp.ny.com.chat.system.App;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by yann-yvan on 16/11/17.
 */

public class Config {

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";
    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static void put(User user, String token) {
        //Get the shared preferences
        SharedPreferences preferences = App.getContext().getSharedPreferences(Properties.USER.toString(), MODE_PRIVATE);
        //Set first_step_complete to true
        preferences.edit().putBoolean(Properties.IS_LOGIN.toString(), true)
                .putString(Properties.TOKEN.toString(), token)
                .putString(Properties.EMAIL.toString(), user.getEmail())
                .putString(Properties.FIRST_NAME.toString(), user.getName())
                .putString(Properties.MOBILE.toString(), user.getPhone())
                .putString(Properties.PASSWORD.toString(), user.getPassword())
                .putInt(Properties.ID.toString(), user.getId())
                .putString(Properties.PICTURE.name(), user.getProfilePicture()).apply();
    }

    public static User getClient() {

        User client = new User();
        //Get the shared preferences
        SharedPreferences preferences = App.getContext().getSharedPreferences(Properties.USER.toString(), MODE_PRIVATE);
        client.setEmail(preferences.getString(Properties.EMAIL.toString(), "NONE"));
        client.setName(preferences.getString(Properties.FIRST_NAME.toString(), "NONE"));
        client.setPassword(preferences.getString(Properties.PASSWORD.toString(), "NONE"));
        client.setId(preferences.getInt(Properties.ID.toString(), 0));
        client.setPhone(preferences.getString(Properties.MOBILE.toString(), "NONE"));
        client.setProfilePicture(preferences.getString(Properties.PICTURE.toString(), "default"));
        client.setDeviceToken(preferences.getString(Properties.DEVICE_ID.toString(), "default"));
        return client;
    }

    public static boolean isLogin() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(Properties.USER.toString(), MODE_PRIVATE);
        return preferences.getBoolean(Properties.IS_LOGIN.toString(), false);
    }

    public static boolean logout() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(Properties.USER.toString(), MODE_PRIVATE);
        return preferences.edit().putBoolean(Properties.IS_LOGIN.toString(), false).commit();
    }

    public static String getToken() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(Properties.USER.toString(), MODE_PRIVATE);
        return preferences.getString(Properties.TOKEN.toString(), "empty");
    }

    public static void setToken(String token) {
        SharedPreferences preferences = App.getContext().getSharedPreferences(Properties.USER.toString(), MODE_PRIVATE);
        preferences.edit().putString(Properties.TOKEN.toString(), token).apply();
    }

    public static String getDeviceID() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(Properties.USER.toString(), MODE_PRIVATE);
        return preferences.getString(Properties.DEVICE_ID.toString(), "empty");
    }

    public static void setDeviceID(String token) {
        SharedPreferences preferences = App.getContext().getSharedPreferences(Properties.USER.toString(), MODE_PRIVATE);
        preferences.edit().putString(Properties.DEVICE_ID.toString(), token).apply();
    }

    public static boolean isCreditCarSet() {
        SharedPreferences preferences = App.getContext().getSharedPreferences(Properties.USER.toString(), MODE_PRIVATE);
        return preferences.getString(Properties.CREDIT_CARD.toString(), null) != null;
    }

    enum Properties {
        USER,
        IS_LOGIN,
        TOKEN,
        EMAIL,
        MOBILE,
        LAST_NAME,
        FIRST_NAME,
        POSTAL_CODE,
        CREDIT_CARD,
        CVV,
        CREDIT_CARD_EXPIRATION_DATE,
        PASSWORD,
        STATUS,
        ID,
        LANGUAGE,
        PICTURE,
        DEVICE_ID
    }
}
