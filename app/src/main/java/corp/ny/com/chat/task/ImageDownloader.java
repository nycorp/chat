package corp.ny.com.chat.task;

import android.content.Context;
import android.os.AsyncTask;

import corp.ny.com.chat.model.Picture;
import corp.ny.com.chat.response.DefaultResponse;

/**
 * Created by yann-yvan on 05/02/18.
 */

public class ImageDownloader extends AsyncTask<Void, Void, DefaultResponse<Picture>> {
    private final int imageID;
    private final Context context;
    private final FireAction fireAction;

    public ImageDownloader(int imageID, Context context, FireAction fireAction) {
        this.imageID = imageID;
        this.context = context;
        this.fireAction = fireAction;
    }

    @Override
    protected DefaultResponse<Picture> doInBackground(Void... params) {
        return null;
    }

    @Override
    protected void onPostExecute(DefaultResponse<Picture> defaultResponse) {
        if (defaultResponse == null)
            return;
    }

    public interface FireAction {
        void onPostExecute(DefaultResponse<Picture> defaultResponse);

        void onFailure();
    }
}
