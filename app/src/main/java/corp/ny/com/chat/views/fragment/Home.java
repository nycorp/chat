package corp.ny.com.chat.views.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;

import corp.ny.com.chat.R;
import corp.ny.com.chat.views.activity.Main;

import static android.view.Gravity.END;
import static android.view.Gravity.START;

public class Home extends Fragment {

    private FragmentTabHost tabHost;
    private TabWidget tabs;
    private FrameLayout tabcontent;
    private LinearLayout tabDiscussion;
    private LinearLayout tabContacts;
    public final static String DISCUSSION_TAG = "tab_discussion";
    public final static String CONTACT_TAG = "tab_contact";

    public static Fragment getInstance() {
        Home f = new Home();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        Home f = new Home();
        f.setArguments(bundle);
        defaultAnimation(f);
        return f;
    }

    private static Fragment defaultAnimation(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setEnterTransition(new Slide(END).setDuration(400));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setExitTransition(new Slide(START).setDuration(400));
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((Main) getActivity()).setSupportActionBar(toolbar);
        final PopupMenu popupMenu = new PopupMenu(getContext().getApplicationContext(),
                toolbar.findViewById(R.id.expanded_menu));
        popupMenu.getMenuInflater().inflate(R.menu.main, popupMenu.getMenu());
        initialize(view);
    }

    private void initialize(View view) {
        tabHost = view.findViewById(R.id.tab_host);
        tabs = view.findViewById(android.R.id.tabs);
        tabcontent = view.findViewById(android.R.id.tabcontent);
        tabDiscussion = view.findViewById(R.id.tab_discussion);
        tabContacts = view.findViewById(R.id.tab_contacts);

        tabHost.setup(getContext(), getFragmentManager(), android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec(DISCUSSION_TAG).setIndicator("Discussions"),
                Conversation.class, null);
        tabHost.addTab(tabHost.newTabSpec(CONTACT_TAG).setIndicator("Contacts"),
                Contacts.class, null);
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
            }
        });
    }

    public FragmentTabHost getTabHost() {
        return tabHost;
    }

    public Fragment getChildFragment(String tabSpecTag) {
        Log.e("Fragment", getFragmentManager().findFragmentByTag(DISCUSSION_TAG).toString());
        return getFragmentManager().findFragmentByTag(tabSpecTag);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }


}
