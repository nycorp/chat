package corp.ny.com.chat.views.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import corp.ny.com.chat.R;
import corp.ny.com.chat.model.User;
import corp.ny.com.chat.task.ContactTask;
import corp.ny.com.chat.views.activity.Main;
import corp.ny.com.chat.views.adapter.ItemContactAdapter;
import corp.ny.com.chat.views.listener.OnItemClickListener;

import static android.view.Gravity.END;
import static android.view.Gravity.START;

public class Contacts extends Fragment implements OnItemClickListener<User> {

    private RecyclerView rvContact;

    public static Fragment getInstance() {
        Contacts f = new Contacts();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        Contacts f = new Contacts();
        f.setArguments(bundle);
        defaultAnimation(f);
        return f;
    }


    private static Fragment defaultAnimation(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setEnterTransition(new Slide(END).setDuration(400));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setExitTransition(new Slide(START).setDuration(400));
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);

    }

    private void initialize(View view) {
        rvContact = view.findViewById(R.id.rv_contact);

        rvContact.setLayoutManager(new LinearLayoutManager(getContext()));
        final ItemContactAdapter contactAdapter = new ItemContactAdapter(getContext());
        ContactTask task = new ContactTask(getContext());
        contactAdapter.addItems(User.getInstance().all());
        task.setListener(new OnItemClickListener<User>() {
            @Override
            public void click(User model) {
                contactAdapter.addItem(model);
            }
        });
        task.execute((Void[]) null);
        contactAdapter.setItemSelectedListener(this);
        rvContact.setAdapter(contactAdapter);
    }

    @Override
    public void click(User model) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(User.class.getName(), model);
        ((Main) getActivity()).addToHistory(ChatRoom.newInstance(bundle));
    }
}
