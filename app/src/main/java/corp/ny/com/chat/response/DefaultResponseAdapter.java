package corp.ny.com.chat.response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by yann-yvan on 19/11/17.
 */

public interface DefaultResponseAdapter {
    void toDefaultWithModel(JSONObject jsonObject, DefaultResponse defaultResponse) throws JSONException, IOException;
}
