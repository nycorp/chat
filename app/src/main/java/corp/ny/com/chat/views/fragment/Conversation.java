package corp.ny.com.chat.views.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import corp.ny.com.chat.R;
import corp.ny.com.chat.model.User;
import corp.ny.com.chat.views.activity.Main;
import corp.ny.com.chat.views.adapter.ItemDiscussionAdapter;
import corp.ny.com.chat.views.listener.OnItemClickListener;

import static android.view.Gravity.END;
import static android.view.Gravity.START;

public class Conversation extends Fragment implements OnItemClickListener<User>, View.OnClickListener {

    private RecyclerView rvDiscussion;

    public static Fragment getInstance() {
        Conversation f = new Conversation();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        Conversation f = new Conversation();
        f.setArguments(bundle);
        defaultAnimation(f);
        return f;
    }


    private static Fragment defaultAnimation(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setEnterTransition(new Slide(END).setDuration(400));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setExitTransition(new Slide(START).setDuration(400));
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversation, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);
    }

    private void initialize(View view) {
        rvDiscussion = view.findViewById(R.id.rv_discussion);
        view.findViewById(R.id.new_discussion).setOnClickListener(this);

        rvDiscussion.setLayoutManager(new LinearLayoutManager(getContext()));
        ItemDiscussionAdapter discussionAdapter = new ItemDiscussionAdapter(getContext());
        discussionAdapter.addItems(User.getInstance().discussion());
        discussionAdapter.setItemSelectedListener(this);
        rvDiscussion.setAdapter(discussionAdapter);
    }

    public RecyclerView getRvDiscussion() {
        return rvDiscussion;
    }

    @Override
    public void click(User model) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(User.class.getName(), model);
        ((Main) getActivity()).addToHistory(ChatRoom.newInstance(bundle));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.new_discussion:
                Fragment fragment = ((Main) getActivity()).getCurrentScreen();
                if (fragment instanceof Home) {
                    ((Home) fragment).getTabHost().setCurrentTab(1);
                }
                break;
        }
    }
}
