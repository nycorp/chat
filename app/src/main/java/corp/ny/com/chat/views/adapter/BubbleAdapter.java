package corp.ny.com.chat.views.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import corp.ny.com.chat.R;
import corp.ny.com.chat.env.Config;
import corp.ny.com.chat.model.Message;
import corp.ny.com.chat.task.MessageTask;
import corp.ny.com.chat.utils.DateManger;
import corp.ny.com.chat.views.listener.OnItemClickListener;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 19/04/18.
 */
public class BubbleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_SEND_SMS = 0;
    private static final int VIEW_TYPE_RECEIVE_SMS = 1;
    public boolean isOldDataAvailable;
    private int lastId = -1;
    private int firstId = 0;
    private List<Message> messages = new ArrayList<Message>();
    private Context context;
    private LayoutInflater layoutInflater;
    private int previousMessage = -1;
    private int currentMessage = VIEW_TYPE_RECEIVE_SMS;
    private RecyclerView recyclerView;
    private String lastPeriod;


    public BubbleAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getItemViewType(int position) {
        return (Config.getClient().getId() != messages.get(position).getSenderId()
                ? VIEW_TYPE_RECEIVE_SMS : VIEW_TYPE_SEND_SMS);
    }

    public void setCurrentMessage(int currentMessage) {
        this.currentMessage = currentMessage;
        this.previousMessage = currentMessage;
    }

    public void addItem(Message message) {
        for (Message sms : messages) {
            if (sms.getHiddenId().equals(message.getHiddenId())) {
                if (message.isSend())
                    sms.delete();
                if (message.isRead()) {
                    final List<Message> unNotifyMessages = new ArrayList<>();
                    unNotifyMessages.add(message);
                    MessageTask task = new MessageTask(unNotifyMessages, context, MessageTask.READ_NOTIFICATION);
                    task.setListener(new OnItemClickListener<Message>() {
                        @Override
                        public void click(Message model) {
                            for (Message m : unNotifyMessages) {
                                m.setRead(true);
                                m.save();
                            }
                        }
                    });
                    task.execute((Void) null);
                }
                message.save();
                int index = messages.indexOf(sms);
                messages.remove(index);
                messages.add(index, message);
                notifyItemChanged(index);
                return;
            }
        }


        if (messages.isEmpty()) {
            messages.add(message);
            notifyItemInserted(messages.size());
        } else {
            messages.add(0, message);
            notifyItemInserted(0);
        }
        if (recyclerView != null) {
            recyclerView.scrollToPosition(0);
        }
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public void addOldItem(List<Message> messages) {
        if (messages == null) {
            isOldDataAvailable = false;
            return;
        }
        if (messages.isEmpty()) {
            isOldDataAvailable = false;
        }

        Log.e("Last ID", "" + lastId);
        for (Message message : messages) {
            if (message.getId() < lastId) lastId = message.getId();
            if (message.getId() > firstId) firstId = message.getId();
            if (this.messages.isEmpty()) {
                lastId = messages.get(0).getId();
            }

            this.messages.add(message);
            notifyItemInserted(this.messages.size());
        }

    }

    public boolean addNewItem(List<Message> messages) {
        if (messages == null) {

            return false;
        }
        if (messages.isEmpty()) {
            return false;
        }
        for (Message message : messages) {
            if (message.getId() > firstId) firstId = message.getId();
            this.messages.add(message);
            notifyItemInserted(this.messages.size());
        }

        return true;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_SEND_SMS:
                return new ViewHolderSend(LayoutInflater.from(context).inflate(R.layout.outgoing_chat, parent, false));
            case VIEW_TYPE_RECEIVE_SMS:
                return new ViewHolderReceive(LayoutInflater.from(context).inflate(R.layout.incoming_chat, parent, false));
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Message message = messages.get(position);
        if (holder instanceof ViewHolderReceive) {
            ViewHolderReceive receive = (ViewHolderReceive) holder;
            receive.text.setText(message.getText());
            receive.setDate(message.getDate());
        } else if (holder instanceof ViewHolderSend) {
            final ViewHolderSend send = (ViewHolderSend) holder;
            send.text.setText(message.getText());
            send.setDate(message.getDate());

            if (message.isRead()) {
                send.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_done_all_black_24dp));
            } else if (message.isSend()) {
                send.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_done_black_24dp));
            } else if (message.isOnProcess()) {
                send.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_access_time));
            } else {
                send.status.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_refresh_black_24dp));
                send.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
                        alert.setItems(R.array.message_action, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.e("which", String.valueOf(which));
                                switch (which) {
                                    case 0:
                                        message.setOnProcess(true);
                                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        message.setDate(dateFormat.format(Calendar.getInstance().getTime()));
                                        notifyItemChanged(send.getAdapterPosition());
                                        MessageTask task = new MessageTask(context, message);
                                        task.setListener(new OnItemClickListener<Message>() {
                                            @Override
                                            public void click(Message model) {
                                                addItem(model);
                                            }
                                        });
                                        task.execute((Void) null);
                                        break;
                                    case 1:
                                        message.delete();
                                        messages.remove(message);
                                        notifyItemRemoved(send.getAdapterPosition());
                                        break;
                                }

                            }
                        });
                        alert.create().show();
                    }
                });
            }
        }
    }


    @Override
    public int getItemCount() {
        return messages.size();
    }


    protected class ViewHolderSend extends RecyclerView.ViewHolder {

        private TextView text;
        private TextView date;
        private TextView period;
        private ImageView status;

        ViewHolderSend(View view) {
            super(view);
            text = view.findViewById(R.id.text);
            date = view.findViewById(R.id.date);
            period = view.findViewById(R.id.period);
            status = view.findViewById(R.id.status);
        }

        public void setDate(String date) {
            if (lastPeriod == null || !lastPeriod.equals(DateManger.format(date))) {
                lastPeriod = DateManger.format(date);
                period.setText(lastPeriod);
                period.setVisibility(View.VISIBLE);
            } else {
                period.setVisibility(View.GONE);
            }
            this.date.setText(date.substring(11, 16));
        }

    }

    protected class ViewHolderReceive extends RecyclerView.ViewHolder {

        private TextView text;
        private TextView date;
        private TextView period;

        ViewHolderReceive(View view) {
            super(view);
            text = view.findViewById(R.id.text);
            date = view.findViewById(R.id.date);
            period = view.findViewById(R.id.period);
        }

        public void setDate(String date) {
            if (date != null) {
                if (lastPeriod == null || !lastPeriod.equals(DateManger.format(date))) {
                    lastPeriod = DateManger.format(date);
                    period.setText(lastPeriod);

                } else {
                    period.setVisibility(View.GONE);
                }
                this.date.setText(date.substring(11, 16));
            } else {
                this.date.setText("--:--");
            }
        }
    }
}
