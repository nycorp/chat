package corp.ny.com.chat.views.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.chat.R;
import corp.ny.com.chat.env.Config;
import corp.ny.com.chat.model.Message;
import corp.ny.com.chat.model.User;
import corp.ny.com.chat.utils.ReleaseKeyBoard;
import corp.ny.com.chat.views.adapter.BubbleAdapter;
import corp.ny.com.chat.views.adapter.ItemDiscussionAdapter;
import corp.ny.com.chat.views.fragment.ChatRoom;
import corp.ny.com.chat.views.fragment.Conversation;
import corp.ny.com.chat.views.fragment.Home;
import corp.ny.com.chat.views.fragment.Login;
import de.hdodenhof.circleimageview.CircleImageView;

public class Main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Fragment currentScreen;
    private boolean isAppLock = true;
    private List<Fragment> history = new ArrayList<>();
    private CircleImageView avatar;
    private BroadcastReceiver broadcastReceiver;
    private static final String TAG = Main.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!Config.isLogin())
            displaySelectedScreen(Login.getInstance());
        else
            displaySelectedScreen(Home.getInstance());

    }

    public CircleImageView getAvatar() {
        return avatar;
    }

    public Toolbar getToolBar() {
        return (Toolbar) findViewById(R.id.toolbar);
    }

    @Override
    public void onBackPressed() {
        if (!history.isEmpty()) {
            getSupportActionBar().show();
            displaySelectedScreen(removeLastScreenFromHistory());
            return;
        }
        super.onBackPressed();

    }

    public void addToHistory(Fragment newScreen) {
        avoidReloadScreen(newScreen);
        history.add(currentScreen);
        displaySelectedScreen(newScreen);
    }

    public Fragment removeLastScreenFromHistory() {
        return history.remove(history.size() - 1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (isAppLock) {
            getMenuInflater().inflate(R.menu.lock_menu, menu);
        } else {
            getMenuInflater().inflate(R.menu.main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_manage_accounts) {
            addToHistory(AccountList.getInstance());
        } else if (id == R.id.action_manage_members) {
            //addToHistory(MemberList.getInstance());
        } else*/
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_about) {
            //addToHistory(About.getInstance());
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void displaySelectedScreen(Fragment screen) {
        ReleaseKeyBoard keyBoard = new ReleaseKeyBoard(getApplicationContext());
        if (currentScreen != null) {
            keyBoard.hide(currentScreen.getView());
        }


        //replacing the fragment
        if (screen != null) {
            avoidReloadScreen(screen);
            currentScreen = screen;
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, screen);
            ft.commitNow();
        }
    }

    private void avoidReloadScreen(Fragment screen) {
        if (screen instanceof ChatRoom && currentScreen instanceof ChatRoom) {
            removeLastScreenFromHistory();
        } else if (screen instanceof Home && currentScreen instanceof Home) {
            removeLastScreenFromHistory();
        }

    }

    public Fragment getCurrentScreen() {
        return currentScreen;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e("Has messages", String.valueOf(intent.getStringExtra("read_notification")));
                if (intent.hasExtra("read_notification")) {
                    try {
                        JSONObject json = new JSONObject(intent.getStringExtra("read_notification"));
                        Log.e("Json", json.toString());
                        JSONObject data = json.getJSONObject("data");
                        Log.e("data", data.toString());
                        Moshi moshi = new Moshi.Builder().build();
                        JsonAdapter<Message> adapter = moshi.adapter(Message.class);
                        Log.e("messages", data.getJSONArray("messages").toString(5));
                        for (int i = 0; i < data.getJSONArray("messages").length(); i++) {
                            Message message = adapter.fromJson(data.getJSONArray("messages").getString(i));
                            if (message != null) {
                                message.setOnProcess(false);
                                message.setiOpen(true);
                                message.setRead(true);
                                message.save();
                                if (currentScreen instanceof ChatRoom) {
                                    message.setiOpen(true);
                                    message.save();
                                    if (((ChatRoom) currentScreen).getUser().getId() == message.getSenderId()) {
                                        ((BubbleAdapter) ((ChatRoom) currentScreen).getRvBubble().getAdapter()).addItem(message);
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        Log.e(TAG, "Exception: " + e.getMessage());
                    }
                    return;
                }

                Message message = (Message) intent.getSerializableExtra("message");
                if (currentScreen instanceof ChatRoom) {
                    message.setiOpen(true);
                    message.save();
                    if (((ChatRoom) currentScreen).getUser().getId() == message.getSenderId()) {
                        ((BubbleAdapter) ((ChatRoom) currentScreen).getRvBubble().getAdapter()).addItem(message);
                    }
                } else if (currentScreen instanceof Home) {
                    if (((Home) currentScreen).getTabHost().getCurrentTabTag().equals(Home.DISCUSSION_TAG))
                        ((ItemDiscussionAdapter)
                                ((Conversation) ((Home) currentScreen)
                                        .getChildFragment(Home.DISCUSSION_TAG))
                                        .getRvDiscussion()
                                        .getAdapter())
                                .addItem(User.getInstance().find(message.getSenderId()));
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter("data"));

        openDiscussion();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        openDiscussion();
    }

    private void openDiscussion() {
        if (getIntent().hasExtra("message")) {
            Bundle bundle = new Bundle();
            bundle.putSerializable(User.class.getName(), User.getInstance().find(((Message) getIntent().getSerializableExtra("message")).getSenderId()));
            addToHistory(ChatRoom.newInstance(bundle));
        }
    }
}
