package corp.ny.com.chat.controllers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import corp.ny.com.chat.env.Route;
import corp.ny.com.chat.response.DefaultResponse;
import corp.ny.com.chat.utils.UnableToAccessInternetException;
import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by yann-yvan on 15/11/17.
 */

public abstract class Controller<T> {

    private MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient client = null;
    private File cacheDir = null;

    Controller(File cacheDir) {
        this.cacheDir = cacheDir;
    }

    public ArrayList makeRequest(String route, String data) throws UnableToAccessInternetException {
        RequestBody body = RequestBody.create(mediaType, data);
        //build our request
        Request request = new Request.Builder().
                url(Route.buildRoute(route)).
                post(body).
                build();
        //get httpclient instance and prepare request execution
        Call callPost = getHttpClient().newCall(request);

        //execute the request and wait for the answer
        Response response;
        try {
            response = callPost.execute();
        } catch (IOException e) {
            throw new UnableToAccessInternetException(e.getMessage());
        }

        ArrayList bigData = new ArrayList();
        //response status
        bigData.add(response.code());
        //your response body
        try {
            bigData.add(response.body().string());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        //you should always close the body to enhance recycling mechanism
        response.body().close();
        //Log.i("server response", bigData.get(1).toString());
        return bigData;
    }

    /**
     * Create new OkHttpClient instance
     *
     * @return OkHttpClient
     */
    private OkHttpClient getHttpClient() {
        if (client == null) {
            //Assigning a CacheDirectory
            File myCacheDir = new File(cacheDir, "OkHttpCache");
            //define cache size
            int cacheSize = 10 * 1024 * 1024; // 10 MiB
            Cache cacheDir = new Cache(myCacheDir, cacheSize);
            client = new OkHttpClient.Builder()
                    .cache(cacheDir)//.readTimeout(15000, TimeUnit.MILLISECONDS)
                    .addInterceptor(getInterceptor())
                    .build();
        }
        //now it's using the cacheresponse.body().string()
        return client;
    }

    /**
     * Convert InputStream into String
     *
     * @param inputStream the target
     * @return string decoded
     */
    private String getStringFromInputStream(InputStream inputStream) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }

    /**
     * Method of creation
     * .readTimeoutMillis()
     *
     * @param obj
     * @return boolean
     */
    public abstract DefaultResponse<T> create(T obj) throws IOException, JSONException, UnableToAccessInternetException;


    /**
     * Method for update
     *
     * @param obj
     * @return
     * @throws IOException
     * @throws JSONException
     */
    public abstract DefaultResponse<T> delete(T obj) throws IOException, JSONException, UnableToAccessInternetException;


    /**
     * Method for update
     *
     * @param obj
     * @return boolean
     * @throws IOException
     * @throws JSONException
     */
    public abstract DefaultResponse<T> update(T obj) throws IOException, JSONException, UnableToAccessInternetException;


    /**
     * Method for find information
     *
     * @param id
     * @return T
     * @throws IOException
     * @throws JSONException
     */
    public abstract DefaultResponse<T> find(String id) throws IOException, JSONException, UnableToAccessInternetException;

    public File getCacheDir() {
        return cacheDir;
    }


    /**
     * How to save a base64 encode bitmap on the disk
     *
     * @param fileName      the bitmap to encode
     * @param base64Picture the base64 string we want to save
     * @throws IOException is throw when there is an error when opening stream for wri
     */
    public String saveBase64Picture(String fileName, String base64Picture, FileDirectoryName subFolder) throws IOException {
        //Second save the picture
        //--------------------------
        //Check if it is a base64 picture
        if (base64Picture.equals("default")) {
            return "default";
        }
        //Find the external storage directory
        File filesDir = getCacheDir();
        //Retrieve the name of the subfolder where your store your picture
        //(You have set it in your string ressources)
        String pictureFolderName = "Pictures/" + subFolder.name().toLowerCase();
        //then create the subfolder
        File pictureDir = new File(filesDir, pictureFolderName);
        //Check if this subfolder exists
        if (!pictureDir.exists()) {
            //if it doesn't create it
            pictureDir.mkdirs();
        }
        //Define the file to store your picture in
        File filePicture = new File(pictureDir, fileName);
        //Open an OutputStream on that file
        FileOutputStream fos = null;
        fos = new FileOutputStream(filePicture);

        //Write in that stream your bitmap in png with the max quality (100 is max, 0 is min quality)
        //convert base 64 image to bitmap
        byte[] decodedString = Base64.decode(base64Picture, Base64.DEFAULT);
        Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        //The close properly your stream
        Log.i("pictures path", filePicture.getPath());

        fos.flush();
        fos.close();

        //return picture path
        return filePicture.getPath();
    }

    /**
     * Convert Bitmap to Base64 encoded for json transfer
     *
     * @param bitmap the bitmap to encode
     * @return the encoded bitmap as string
     */
    public String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 70, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }


    /**
     * How to save a Bitmap on the disk
     *
     * @param fileName the name of the file we want to save
     * @param bitmap   the bitmap we want to save
     * @throws IOException is throw when there is an error when opening stream for write file
     */
    public String saveBitmapPicture(String fileName, Bitmap bitmap) throws IOException {
        //Second save the picture
        //--------------------------
        //Find the external storage directory
        File filesDir = getCacheDir();
        //Retrieve the name of the subfolder where your store your picture
        //(You have set it in your string ressources)
        String pictureFolderName = "Pictures";
        //then create the subfolder
        File pictureDir = new File(filesDir, pictureFolderName);
        //Check if this subfolder exists
        if (!pictureDir.exists()) {
            //if it doesn't create it
            pictureDir.mkdirs();
        }
        //Define the file to store your picture in
        File filePicture = new File(pictureDir, fileName);
        //Open an OutputStream on that file
        FileOutputStream fos = new FileOutputStream(filePicture);
        //Write in that stream your bitmap in png with the max quality (100 is max, 0 is min quality)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        //The close properly your stream
        Log.i("pictures path", filePicture.getPath());
        fos.flush();
        fos.close();
        //return picture pathreturn response()->json(['status' => false,
        return filePicture.getPath();
    }

    private Interceptor getInterceptor() {
        return new LoggingInterceptor();
    }

    public enum FileDirectoryName {
        PROFILE,
        CAR
    }

    /**
     * this class helps us to log all ours requests
     */
    private class LoggingInterceptor implements Interceptor {
        //Code pasted from okHttp webSite itself
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            long t1 = System.nanoTime();
            Log.i("Interceptor request", String.format("Sending request %s on %s%n%s",
                    request.url(), chain.connection(), request.headers()));
            Response response = chain.proceed(request);
            long t2 = System.nanoTime();
            Log.i("Interceptor request", String.format("Received response for %s in .1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6d, response.headers()));
            Log.d("Server response", response.toString());
            return response;
        }
    }
}
