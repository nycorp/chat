package corp.ny.com.chat.views.fragment;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.jetbrains.annotations.Nullable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import corp.ny.com.chat.R;
import corp.ny.com.chat.env.Config;
import corp.ny.com.chat.model.Message;
import corp.ny.com.chat.model.User;
import corp.ny.com.chat.task.MessageTask;
import corp.ny.com.chat.views.activity.Main;
import corp.ny.com.chat.views.adapter.BubbleAdapter;
import corp.ny.com.chat.views.listener.OnItemClickListener;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.Gravity.END;
import static android.view.Gravity.START;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 19/04/18.
 */


public class ChatRoom extends Fragment implements View.OnClickListener {
    private RecyclerView rvBubble;
    private EditText message;
    private FloatingActionButton fbSendMessage;
    private Toolbar toolbar;
    private ImageView btnBack;
    private CircleImageView image;
    private TextView name;
    private TextView status;
    private User user;

    public static Fragment getInstance() {
        ChatRoom f = new ChatRoom();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        ChatRoom f = new ChatRoom();
        f.setArguments(bundle);
        defaultAnimation(f);
        return f;
    }

    private static Fragment defaultAnimation(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setEnterTransition(new Slide(END).setDuration(400));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setExitTransition(new Slide(START).setDuration(400));
        }
        return fragment;
    }

    public User getUser() {
        return user;
    }

    public RecyclerView getRvBubble() {
        return rvBubble;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chat_room, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((Main) getActivity()).setSupportActionBar(toolbar);
        final PopupMenu popupMenu = new PopupMenu(getContext().getApplicationContext(),
                toolbar.findViewById(R.id.expanded_menu));
        popupMenu.getMenuInflater().inflate(R.menu.main, popupMenu.getMenu());
        initialize(view);
    }

    private void initialize(View view) {
        findViews(view);
        BubbleAdapter bubbleAdapter = new BubbleAdapter(getContext());
        rvBubble.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true));
        rvBubble.setAdapter(bubbleAdapter);

        message.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //Toast.makeText(getContext(), ""+message.getText().length(), Toast.LENGTH_SHORT).show();
                if (message.getText().toString().isEmpty()) {
                    fbSendMessage.setEnabled(false);
                } else {
                    fbSendMessage.setEnabled(true);
                }
            }
        });

        if (getArguments() != null) {
            if (getArguments().containsKey(User.class.getName())) {
                user = (User) getArguments().get(User.class.getName());
                assert user != null;
                name.setText(user.getName());
            }
        }
        bubbleAdapter.setRecyclerView(rvBubble);
        bubbleAdapter.addOldItem(Message.getInstance().discussionMessage(String.valueOf(user.getId())));
        btnBack.setOnClickListener(this);
        image.setOnClickListener(this);

        //update unread message to read
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (Message message : Message.getInstance().getUnreadMessage(user.getId())) {
                    message.setiOpen(true);
                    message.update();
                }
            }
        };
        runnable.run();
        final List<Message> unNotifyMessages = Message.getInstance().getUnNotifyReadMessage(user.getId());
        if (!unNotifyMessages.isEmpty()) {
            MessageTask task = new MessageTask(unNotifyMessages, getContext(), MessageTask.READ_NOTIFICATION);
            task.setListener(new OnItemClickListener<Message>() {
                @Override
                public void click(Message model) {
                    for (Message m : unNotifyMessages) {
                        m.setRead(true);
                        m.save();
                    }
                }
            });
            task.execute((Void) null);
        }
    }

    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        rvBubble = container.findViewById(R.id.rv_bubble);
        message = container.findViewById(R.id.text);
        fbSendMessage = container.findViewById(R.id.fb_sendMessage);
        toolbar = container.findViewById(R.id.toolbar);
        btnBack = container.findViewById(R.id.btn_back);
        image = container.findViewById(R.id.image);
        name = container.findViewById(R.id.name);
        status = container.findViewById(R.id.status);

        fbSendMessage.setOnClickListener(this);
    }

    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == fbSendMessage) {
            Message message = new Message();
            message.setText(this.message.getText().toString());
            message.setSenderId(Config.getClient().getId());
            message.setReceiverId(user.getId());
            List<Message> messageList = message.allById();
            message.setId((messageList.isEmpty() ? 1 : messageList.get(0).getId() + 1));
            message.setHiddenId(String.valueOf(Calendar.getInstance().getTime().getTime()));
            message.setOnProcess(true);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            message.setDate(dateFormat.format(Calendar.getInstance().getTime()));
            message.setiOpen(true);
            message.save();
            MessageTask task = new MessageTask(getContext(), message);
            ((BubbleAdapter) rvBubble.getAdapter()).addItem(message);
            task.setListener(new OnItemClickListener<Message>() {
                @Override
                public void click(Message model) {
                    ((BubbleAdapter) rvBubble.getAdapter()).addItem(model);
                }
            });
            task.execute((Void) null);
            this.message.setText("");
            // Handle clicks for fbSendMessage
        } else if (v == btnBack || v == image) {
            getActivity().onBackPressed();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }
}
