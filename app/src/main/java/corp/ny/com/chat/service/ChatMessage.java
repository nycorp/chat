package corp.ny.com.chat.service;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.json.JSONException;
import org.json.JSONObject;

import corp.ny.com.chat.env.Config;
import corp.ny.com.chat.model.Message;
import corp.ny.com.chat.model.User;
import corp.ny.com.chat.task.ContactTask;
import corp.ny.com.chat.utils.UNotification;
import corp.ny.com.chat.views.activity.Main;
import corp.ny.com.chat.views.listener.OnItemClickListener;

/**
 * Created by yann-yvan on 16/12/17.
 */

public class ChatMessage extends FirebaseMessagingService {
    private static final String TAG = ChatMessage.class.getSimpleName();

    private UNotification notificationUtils;
    private LocalBroadcastManager broadcaster;
    private RemoteMessage remoteMessage;

    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                this.remoteMessage = remoteMessage;
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        if (!UNotification.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            UNotification notificationUtils = new UNotification(getApplicationContext());
            notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification

        }
    }

    private void handleDataMessage(JSONObject json) {
        try {
            Log.e(TAG, "push json: " + json.toString(5));
            JSONObject data = json.getJSONObject("data");
            Moshi moshi = new Moshi.Builder().build();
            Intent intent = new Intent("data");
            String title = data.getString("title");
            JsonAdapter<Message> adapter = moshi.adapter(Message.class);
            String jsonData = remoteMessage.getData().toString();
            boolean isSilent = data.has("context");

            Message sms = Message.getInstance();
            String message = "";
            String timestamp = "";
            if (!isSilent) {
                sms = adapter.fromJson(data.getString("message"));
                if (sms == null) return;
                message = sms.getText();
                timestamp = sms.getDate();
                if (User.getInstance().find(sms.getSenderId()) == null) {
                    ContactTask task = new ContactTask(getApplicationContext());
                    final Message message1 = sms;
                    task.setListener(new OnItemClickListener<User>() {
                        @Override
                        public void click(User model) {
                            if (model.getId() == message1.getSenderId())
                                message1.save();
                        }
                    });
                    task.execute((Void) null);
                } else
                    sms.save();
                intent.putExtra("message", sms);
            } else {
                intent.putExtra("read_notification", jsonData);
            }

            int imageID = 0;
            if (data.has("image_id")) {
                imageID = data.getInt("image_id");
            }

//            Log.e(TAG, "title: " + title);
//            Log.e(TAG, "imageID: " + imageID);
//            Log.e(TAG, "timestamp: " + timestamp);


            if (!UNotification.isAppIsInBackground(getApplicationContext())) {
                if (isSilent) {
                    Log.e(TAG, "Is Silent: " + title);
                    // app is in foreground, broadcast the push message
                    Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                    pushNotification.putExtra("read_notification", jsonData);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                    broadcaster.sendBroadcast(intent);
                } else {
                    // app is in foreground, broadcast the push message
                    Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                    pushNotification.putExtra("message", sms);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                    // play notification sound
                    UNotification notificationUtils = new UNotification(getApplicationContext());
                    notificationUtils.playNotificationSound();
                    broadcaster.sendBroadcast(intent);
                }
            } else {
                if (isSilent) {
                    return;
                }
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), Main.class);
                resultIntent.putExtra("message", sms);

                // check for image attachment
                if (imageID < 1) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageID);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new UNotification(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, int imageID) {
        notificationUtils = new UNotification(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageID);
    }
}
