package corp.ny.com.chat.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

import corp.ny.com.chat.system.App;
import corp.ny.com.chat.utils.ClassBuilder;

import static corp.ny.com.chat.utils.ClassBuilder.fillAttribute;


/**
 * Created by yann-yvan on 05/12/17.
 * Current DATABASE  VERSION = 1
 */

public abstract class Model<T> implements Cloneable, Serializable {

    // the name of the file of our database
    public final static String NAME = "chat.db";

    // Increment this value if you to update database
    public final static int VERSION = 1;

    //getTable() identify
    private String idName = "id";
    //limit
    private int limit = 5;
    //handled model
    //in case of need of cursor value
    //private Cursor cloneCursor;

    /**
     * Current database version
     *
     * @return an integer that represent <b>database version</b>
     */
    public static int getVERSION() {
        return VERSION;
    }

    /**
     * Get database instance
     *
     * @return {@linkplain SQLiteDatabase} instance
     */
    protected SQLiteDatabase getDb() {
        return App.getDataBaseInstance();
    }

    /**
     * Define table name
     *
     * @return table name
     */
    public String getTableName() {
        return ((T) this).getClass().getSimpleName().toLowerCase();
    }

    /**
     * Define by which column result should be order
     *
     * @return column name
     */
    public String getOrderBy() {
        return idName;
    }

    /**
     * Get the value of the used model
     *
     * @return id value
     */
    public abstract String getIdValue();

    /**
     * Define table identifier column
     *
     * @return identify column name
     * DefResponse identify column name is <b>id</b>
     */
    public String getIdName() {
        return idName;
    }

    /**
     * Define table limit result per query by range
     *
     * @return the number of row to return per query
     * DefResponse value is <b>5</b>
     */
    public int getLimit() {
        return limit;
    }


    /**
     * Insert values into a table
     *
     * @return the model inserted into the table on <b>null</b> if something went wrong
     */
    public T save() {
        try {
            long success = getDb().insertWithOnConflict(getTableName(), null, sqlQueryBuilder(new ContentValues()), SQLiteDatabase.CONFLICT_FAIL);
            if (success > 0) {
                Log.e("Save ", String.valueOf(success));
                return find(success);
            }
        } catch (SQLiteConstraintException e) {
            //e.printStackTrace();
            return update();
        }
        return null;
    }

    /**
     * Get last select query cursor
     *
     * @return a cursor
     */
   /* public Cursor getCloneCursor() {
        return cloneCursor;
    }*/

    /**
     * Method for delete
     *
     * @return boolean true if success
     */
    public boolean delete() {
        int success = getDb().delete(getTableName(), getIdName() + "=?", new String[]{getIdValue()});
        return success > 0;
    }

    /**
     * Count the total of row in the table
     *
     * @return total of row
     */
    public int count() {
        int total = 0;
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s", getTableName()), null);
        if (cursor != null) {
            //cloneCursor = cursor;
            total = cursor.getCount();
            cursor.close();
        }
        return total;
    }


    /**
     * Update a model in the table
     *
     * @return the model up to date from the table on <b>null</b> if something went wrong
     */
    public T update() {
        try {
            int success = getDb().update(getTableName(), sqlQueryBuilder(new ContentValues()), getIdName() + "=?", new String[]{getIdValue()});
            if (success > 0) {
                Log.e("new Id ", String.valueOf(success));
                return find(success);
            }
        } catch (SQLiteConstraintException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Method to find model by <i>id</i>
     *
     * @param id represent the unique value that identify a row or model
     * @return the model found or <b>null</b> if nothing found in table
     */
    public T find(String id) {
        Cursor cursor = getDb().query(getTableName(), null, getIdName() + " LIKE ?",
                new String[]{id}, null, null, null);
        if (cursor != null) {
            //cloneCursor = cursor;
            if (cursor.moveToNext()) {
                return cursorToModel(cursor);
            }
            cursor.close();
        }
        return null;
    }

    /**
     * Method to find model by <i>id</i>
     *
     * @param id represent the unique value that identify a row or model
     * @return the model found or <b>null</b> if nothing found in table
     */
    public T find(int id) {
        Cursor cursor = getDb().query(getTableName(), null, getIdName() + " LIKE ?",
                new String[]{String.valueOf(id)}, null, null, null);
        if (cursor != null) {
            //cloneCursor = cursor;
            if (cursor.moveToNext()) {
                return cursorToModel(cursor);
            }
            cursor.close();
        }
        return null;
    }

    /**
     * Method to find model by <i>id</i>
     *
     * @param id represent the unique value that identify a row or model
     * @return the model found or <b>null</b> if nothing found in table
     */
    public T find(long id) {
        Cursor cursor = getDb().query(getTableName(), null, getIdName() + " LIKE ?",
                new String[]{String.valueOf(id)}, null, null, null);
        if (cursor != null) {
            //cloneCursor = cursor;
            if (cursor.moveToNext()) {
                return cursorToModel(cursor);
            }
            cursor.close();
        }
        return null;
    }

    /**
     * Method to find all model from a table
     *
     * @return a list of model found or an<b>empty list</b> if nothing found in table
     */
    public ArrayList<T> all() {
        ArrayList<T> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getOrderBy()), null);
        if (cursor != null) {
            //cloneCursor = cursor;
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    /**
     * Method for find information by lastID and get result list
     *
     * @param lastID the id of the last model
     * @return a list of model found starting from the value nested the last id or an<b>empty list</b> if nothing found in table
     * <br>the list is paginate <b>default value is 5 per result</b>
     */
    public ArrayList<T> findByRange(String lastID) {
        ArrayList<T> result = new ArrayList<>();
        System.out.print(lastID);
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s > ? ORDER BY %s LIMIT %s", getTableName(), getIdName(), getOrderBy(), getLimit()),
                new String[]{lastID});
        if (cursor != null) {
            //cloneCursor = cursor;
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    /**
     * Method for find information by lastID and get result list
     *
     * @param lastID the id of the last model
     * @return a list of model found starting from the value nested the last id or an<b>empty list</b> if nothing found in table
     * <br>the list is paginate <b>default value is 5 per result</b>
     */
    public ArrayList<T> findByRange(int lastID) {
        ArrayList<T> result = new ArrayList<>();
        System.out.print(lastID);
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s > ? ORDER BY %s LIMIT %s", getTableName(), getIdName(), getOrderBy(), getLimit()),
                new String[]{String.valueOf(lastID)});
        if (cursor != null) {
            // cloneCursor = cursor;
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    /**
     * Method for find information by lastID and get result list
     *
     * @param lastID the id of the last model
     * @return a list of model found starting from the value nested the last id or an<b>empty list</b> if nothing found in table
     * <br>the list is paginate <b>default value is 5 per result</b>
     */
    public ArrayList<T> findByRangeInvert(int lastID) {
        ArrayList<T> result = new ArrayList<>();
        System.out.print(lastID);
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s < ? ORDER BY %s LIMIT %s", getTableName(), getIdName(), getOrderBy(), getLimit()),
                new String[]{String.valueOf(lastID)});
        if (cursor != null) {
            //cloneCursor = cursor;
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    public String toFCUpperCase(String e) {
        String finalString = "";
        e = e.trim();
        for (String str : e.split(" ")) {
            finalString = finalString.trim();
            //finalString.concat(finalString.substring(0, 1).toUpperCase()
        }
        return e;
    }

    /**
     * Convert a model to sql query
     * <br> <b>example</b><br>
     * <quote>
     * ContentValues query = new ContentValues();<br>
     * if (obj.getId() != 0) {
     * query.put("id", obj.getId());
     * }<br>
     * query.put("types_id", obj.getTypeID());<br>
     * query.put("name", obj.getName());<br>
     * return query;<br>
     * </code>
     *
     * @param query will contain the value of the of the model in query string
     * @return and object that will be use for update and insert query
     */
    public ContentValues sqlQueryBuilder(ContentValues query) {
        ClassBuilder<T> classBuilder = new ClassBuilder<>();
        return classBuilder.prepareStatement((T) this, query);
    }

    /**
     * Convert a row result to a model
     * <p> <b>example</b><br>
     * <blockquote>
     * <pre>
     * return new Model(cursor.getInt(0), cursor.getInt(1), cursor.getString(2));
     * </pre></blockquote>
     *
     * @param cursor object containing all query rows
     * @return the model with all attribute like save in the table
     */
    public T cursorToModel(Cursor cursor) {
        T object = null;
        try {
            object = (T) clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }

        try {
            assert object != null;
            fillAttribute(object, cursor);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        return object;
    }

    /**
     * @return
     */
    public String buildTable() {
        Schema schema = Schema.instantiate(getTableName());
        tableStructure(schema);
        return schema.toString();
    }

    /**
     * <blockquote>
     * <b>Sample</b>
     * <pre>
     * table.increments("id");
     * table.string("name");
     * table.string("phone", 15).nullable().defValue("6 XX XX XX XX");
     * table.string("email").unique();
     * table.string("deviceToken").nullable();
     * table.string("profilePicture").nullable();
     * table.string("password").nullable();
     * </pre></blockquote>
     *
     * @param table
     */
    public abstract void tableStructure(@NonNull Schema table);

    @Override
    public String toString() {
        return new ClassBuilder<T>().printClass((T) this);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
