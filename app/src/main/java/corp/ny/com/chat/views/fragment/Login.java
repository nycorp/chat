package corp.ny.com.chat.views.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.Nullable;
import org.json.JSONException;

import java.io.IOException;

import corp.ny.com.chat.R;
import corp.ny.com.chat.controllers.UserController;
import corp.ny.com.chat.env.Config;
import corp.ny.com.chat.model.User;
import corp.ny.com.chat.response.DefaultResponse;
import corp.ny.com.chat.task.UserTask;
import corp.ny.com.chat.utils.EntryManager;
import corp.ny.com.chat.utils.UnableToAccessInternetException;
import corp.ny.com.chat.views.activity.Main;

import static corp.ny.com.chat.response.RequestCode.authMessage;
import static corp.ny.com.chat.utils.EntryManager.displayError;
import static corp.ny.com.chat.utils.EntryManager.hideError;


public class Login extends Fragment implements View.OnClickListener {
    private FrameLayout appbar;
    private ScrollView signProgress;
    private LinearLayout signForm;
    private EditText emailSign;
    private TextView emailError;
    private EditText passwordSign;
    private TextView passwordError;
    private Button btnLogin;
    private TextView linkForgotPasswd;
    private ProgressDialog progressDialog;
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    //global access
    private int attempt = 0;

    public static Fragment getInstance() {
        Login f = new Login();
        defaultAnimation(f);
        return f;
    }

    public static Fragment newInstance(Bundle bundle) {
        Login f = new Login();
        f.setArguments(bundle);
        defaultAnimation(f);
        return f;
    }

    private static Fragment defaultAnimation(Fragment fragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setEnterTransition(new Slide(Gravity.END).setDuration(400));
            fragment.setExitTransition(new Slide(Gravity.START).setDuration(400));
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize(view);
    }

    private void initialize(View view) {
        findViews(view);
    }

    /**
     * Find the Views in the layout<br />
     */
    private void findViews(View container) {
        appbar = container.findViewById(R.id.appbar);
        signForm = container.findViewById(R.id.sign_form);
        emailSign = container.findViewById(R.id.email_sign);
        emailError = container.findViewById(R.id.email_error);
        passwordSign = container.findViewById(R.id.password_sign);
        passwordError = container.findViewById(R.id.password_error);
        btnLogin = container.findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(this);


        emailSign.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                hideError(emailSign, emailError);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        passwordSign.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                hideError(passwordSign, passwordError);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    /**
     * Use this function to hide all error TextView
     */
    public void resetAllError() {
        hideError(emailSign, emailError);
        hideError(passwordSign, passwordError);
    }

    public boolean getError() {
        if (!EntryManager.emailValidator(emailSign.getText().toString().trim())) {
            if (TextUtils.isEmpty(emailSign.getText().toString().trim())) {
                emailError.setText(R.string.error_field_required);
            } else {
                emailError.setText(R.string.error_invalid_email);
            }
            displayError(emailSign, emailError);
            return true;
        }

        if (!EntryManager.passwordValidator(passwordSign.getText().toString())) {
            if (TextUtils.isEmpty(passwordSign.getText().toString())) {
                passwordError.setText(R.string.error_field_required);
            } else {
                passwordError.setText(R.string.error_invalid_password);
            }
            displayError(passwordSign, passwordError);
            return true;
        }

        return false;
    }

    /**
     * Handle button click events<br />
     */
    @Override
    public void onClick(View v) {
        if (v == btnLogin) {
            // Handle clicks for btnLogin
            resetAllError();
            if (!getError()) {
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                mAuthTask = new UserLoginTask(emailSign.getText().toString(), passwordSign.getText().toString());
                mAuthTask.execute((Void) null);
            }
        }
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    private class UserLoginTask extends AsyncTask<Void, Void, DefaultResponse> {

        private final String mEmail;
        private final String mPassword;
        private boolean noInternet = false;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected DefaultResponse doInBackground(Void... params) {
            //attempt authentication against a network service.
            try {
                UserController controller = new UserController(getActivity().getCacheDir());
                return controller.login(mEmail, mPassword);
            } catch (JSONException e) {
                noInternet = true;
                attempt++;
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("no internet", "Unable to reach server");
            } catch (UnableToAccessInternetException e) {
                noInternet = true;
                attempt++;
            }
            return null;
        }

        @Override
        protected void onPostExecute(final DefaultResponse defaultResponse) {
            mAuthTask = null;
            progressDialog.dismiss();

            //Alert the user to switch on the internet access
            if (noInternet) {
                if (attempt == 3) {
                    //noInternetAlert(Login.this);
                    attempt = 0;
                } else {
                    Toast.makeText(getContext(), "Unable to connect to internet", Toast.LENGTH_SHORT).show();
                }
            }

            //check is the request has not failed or bad server response
            if (defaultResponse == null) {
                //Log.e("bad request", "something when wrong");
                return;
            }

            Log.i("message", authMessage(defaultResponse.getMessage()).toString());

            //make an action that will Alert the user
            switch (authMessage(defaultResponse.getMessage())) {

                case WRONG_USERNAME:
                    emailError.setText(getString(R.string.wrong_username));
                    displayError(emailSign, emailError);
                    emailSign.requestFocus();
                    break;

                case WRONG_PASSWORD:
                    passwordError.setText(getString(R.string.wrong_password));
                    displayError(passwordSign, passwordError);
                    passwordSign.requestFocus();
                    break;

                case LOGIN_SUCCESSFULLY:
                    ((User) defaultResponse.getModel()).setPassword(mPassword);
                    UserTask task = new UserTask(getContext());
                    task.execute((Void[]) null);
                    Config.put((User) defaultResponse.getModel(), defaultResponse.getToken());
                    ((Main) getActivity()).displaySelectedScreen(Home.getInstance());
                    break;

            }

        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            progressDialog.dismiss();
        }
    }
}
