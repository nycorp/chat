package corp.ny.com.chat.model;

import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import corp.ny.com.chat.database.Constraint;
import corp.ny.com.chat.database.Model;
import corp.ny.com.chat.database.Schema;
import corp.ny.com.chat.env.Config;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 19/04/18.
 */
public class Message extends Model<Message> {
    private int id;
    private String date;
    private String hiddenId;
    private String text;
    private int receiverId;
    private int senderId;
    private boolean isRead;
    private boolean isSend;
    private boolean isOnProcess;
    private boolean iOpen;

    public static Message getInstance() {
        return new Message();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isSend() {
        return isSend;
    }

    public void setSend(boolean send) {
        isSend = send;
    }

    public boolean isOnProcess() {
        return isOnProcess;
    }

    public void setOnProcess(boolean onProcess) {
        isOnProcess = onProcess;
    }

    public boolean isiOpen() {
        return iOpen;
    }

    public void setiOpen(boolean iOpen) {
        this.iOpen = iOpen;
    }

    public String getHiddenId() {
        return hiddenId;
    }

    public void setHiddenId(String hiddenId) {
        this.hiddenId = hiddenId;
    }

    @Override
    public String getIdValue() {
        return String.valueOf(id);
    }

    @Override
    public void tableStructure(@NonNull Schema table) {
        table.integer("id");
        table.primary("id");
        table.text("text");
        table.integer("senderId");
        table.integer("receiverId");
        table.string("date");
        table.string("hiddenId");
        table.bool("isOnProcess").defValue(true);
        table.bool("isSend").defValue(false);
        table.bool("isRead").defValue(false);
        table.bool("iOpen").defValue(false);
        table.foreign("senderId")
                .references(User.getInstance().getIdName())
                .on(User.getInstance().getTableName())
                .onDelete(Constraint.Action.CASCADE)
                .onUpdate(Constraint.Action.CASCADE);
        table.foreign("receiverId")
                .references(User.getInstance().getIdName())
                .on(User.getInstance().getTableName())
                .onDelete(Constraint.Action.CASCADE)
                .onUpdate(Constraint.Action.CASCADE);
    }

    public ArrayList<Message> discussionMessage(String id) {
        ArrayList<Message> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s = ? AND %s = ? UNION SELECT * FROM %s WHERE %s = ? AND %s = ? ORDER BY %s", getTableName(), "receiverId", "senderId", getTableName(), "senderId", "receiverId", getOrderBy()),
                new String[]{String.valueOf(Config.getClient().getId()), id, String.valueOf(Config.getClient().getId()), id});
        if (cursor != null) {
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    public Message lastDiscussionMessage(int id) {
        ArrayList<Message> result = discussionMessage(String.valueOf(id));
        if (result.isEmpty()) return Message.getInstance();
        return result.get(0);
    }

    public int countUnreadMessage(int id) {
        int total = -1;
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s = ? AND %s = ? ", getTableName(), "senderId", "iOpen"),
                new String[]{String.valueOf(id), "0"});
        if (cursor != null) {
            total = cursor.getCount();
            cursor.close();
        }
        return total;
    }

    public ArrayList<Message> getUnreadMessage(int id) {
        ArrayList<Message> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s = ? AND %s = ? ", getTableName(), "senderId", "iOpen"),
                new String[]{String.valueOf(id), "0"});
        if (cursor != null) {
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    public ArrayList<Message> getUnNotifyReadMessage(int id) {
        ArrayList<Message> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s WHERE %s = ? AND %s = ? ", getTableName(), "senderId", "isRead"),
                new String[]{String.valueOf(id), "0"});
        if (cursor != null) {
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    public ArrayList<Message> allById() {
        ArrayList<Message> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(String.format("SELECT * FROM %s ORDER BY %s", getTableName(), "id DESC"), null);
        if (cursor != null) {
            //cloneCursor = cursor;
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

    @Override
    public String getOrderBy() {
        return "date DESC";
    }
}
