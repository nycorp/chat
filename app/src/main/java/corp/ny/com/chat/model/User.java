package corp.ny.com.chat.model;

import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import corp.ny.com.chat.database.Model;
import corp.ny.com.chat.database.Schema;
import corp.ny.com.chat.env.Config;

/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 20/04/18.
 */
public class User extends Model<User> {

    private int id;
    private String name;
    private String email;
    private String password;
    private String deviceToken;
    private String profilePicture;
    private String phone;

    public static User getInstance() {
        return new User();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    @Override
    public void tableStructure(@NonNull Schema table) {
        table.increments("id");
        table.string("name");
        table.string("phone", 15).nullable().defValue("6 XX XX XX XX");
        table.string("email").unique();
        table.string("deviceToken").nullable();
        table.string("profilePicture").nullable();
        table.string("password").nullable();
    }

    @Override
    public String getIdValue() {
        return String.valueOf(id);
    }

    @Override
    public String getOrderBy() {
        return "name";
    }

    public ArrayList<User> discussion() {
        ArrayList<User> result = new ArrayList<>();
        Cursor cursor = getDb().rawQuery(
                String.format("SELECT DISTINCT * FROM %s WHERE id " +
                                "IN ( SELECT senderId FROM %s " +
                                "WHERE %s = ? " +
                                "UNION " +
                                "SELECT receiverId FROM %s " +
                                "WHERE %s = ? )" +
                                "ORDER BY %s"
                        , getTableName()
                        , Message.getInstance().getTableName()
                        , "receiverId"
                        , Message.getInstance().getTableName()
                        , "senderId"
                        , getOrderBy())
                , new String[]{String.valueOf(Config.getClient().getId()),
                        String.valueOf(Config.getClient().getId())});
        if (cursor != null) {
            while (cursor.moveToNext()) {
                result.add(cursorToModel(cursor));
            }
            cursor.close();
        }
        return result;
    }

}
