package corp.ny.com.chat.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import corp.ny.com.chat.controllers.MessageController;
import corp.ny.com.chat.model.Message;
import corp.ny.com.chat.response.DefaultResponse;
import corp.ny.com.chat.utils.UnableToAccessInternetException;
import corp.ny.com.chat.views.listener.OnItemClickListener;

import static corp.ny.com.chat.response.RequestCode.requestMessage;
import static corp.ny.com.chat.response.RequestCode.tokenMessage;


/**
 * Created by yann-yvan on 03/01/18.
 */

public class MessageTask extends AsyncTask<Void, Void, DefaultResponse<Message>> {

    public final static int READ_NOTIFICATION = 1;
    private final Context context;
    private Message message;
    private OnItemClickListener<Message> listener;
    private List<Message> messageList;
    private int MESSAGE_CONTEXT = 0;


    public MessageTask(Context context, Message message) {
        this.context = context;
        this.message = message;
    }

    public MessageTask(List<Message> messageList, Context context, int MESSAGE_CONTEXT) {
        this.messageList = messageList;
        this.context = context;
        this.MESSAGE_CONTEXT = MESSAGE_CONTEXT;
        Log.e("MESSAGE_CONTEXT", String.valueOf(MESSAGE_CONTEXT));
    }

    public void setListener(OnItemClickListener<Message> listener) {
        this.listener = listener;
    }

    @Override
    protected DefaultResponse<Message> doInBackground(Void... params) {
        MessageController controller = new MessageController(context.getCacheDir());

        try {
            switch (MESSAGE_CONTEXT) {
                case READ_NOTIFICATION:
                    return controller.readNotification(messageList);
                default:
                    return controller.create(message);
            }
        } catch (IOException | JSONException | UnableToAccessInternetException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(DefaultResponse<Message> messageDefaultResponse) {
        //check is the request has not failed or bad server response
        if (messageDefaultResponse == null) {
            //Log.e("bad request", "something when wrong");
            switch (MESSAGE_CONTEXT) {
                case READ_NOTIFICATION:
                    return;
                default:
                    message.setOnProcess(false);
                    message.setSend(false);
                    if (listener != null)
                        listener.click(message);
                    return;
            }

        }

        switch (requestMessage(messageDefaultResponse.getMessage())) {
            case SUCCESS:
                Log.e("Message send success", "success");
                switch (MESSAGE_CONTEXT) {
                    case READ_NOTIFICATION:
                        if (listener != null)
                            listener.click(Message.getInstance());
                        break;
                    default:
                        message.setOnProcess(false);
                        if (listener != null)
                            listener.click(messageDefaultResponse.getModel());
                }
                break;

            case FAILURE:
                Log.e("Update", "fails");
                break;

            case UNKNOWN_CODE:

                switch (tokenMessage(messageDefaultResponse.getMessage())) {
                    case NO_TOKEN:

                        break;
                    case INVALID_TOKEN:

                        break;
                    case TOKEN_EXPIRED:

                        break;
                    case USER_NOT_FOUND:

                        break;
                    case UNKNOWN_CODE:

                        break;
                }
                break;
        }

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
