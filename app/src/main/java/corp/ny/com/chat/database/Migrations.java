package corp.ny.com.chat.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import corp.ny.com.chat.model.Message;
import corp.ny.com.chat.model.User;

/**
 * Created by yann-yvan on 04/12/17.
 */

public class Migrations extends SQLiteOpenHelper {

    public Migrations(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("Table User", User.getInstance().buildTable());
        Log.d("Table Message", Message.getInstance().buildTable());
        db.execSQL(User.getInstance().buildTable());
        db.execSQL(Message.getInstance().buildTable());
        /*db.execSQL(MemberTable.buildTable());//member Model creation
        db.execSQL(AccountTypeTable.buildTable());//account type Model creation
        db.execSQL(AccountTypeTable.insertDefaultData());//fill account type Model with default data
        db.execSQL(AccountTable.buildTable());//account table creation
        db.execSQL(AccountTable.insertDefaultData());//fill account Model with default data
        db.execSQL(ParticipationTable.buildTable());//participation type Model creation
        db.execSQL(OperationTypeTable.buildTable());//operation type Model creation
        db.execSQL(OperationTypeTable.insertDefaultData());//fill operation type Model with default data
        db.execSQL(OperationTable.buildTable());//operation table creation*/
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            db.setForeignKeyConstraintsEnabled(true);
        }
        super.onConfigure(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void onLogout(SQLiteDatabase db) {
        onCreate(db);
    }

}
