package corp.ny.com.chat.utils;


import android.annotation.TargetApi;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Build;
import android.support.annotation.Nullable;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public class ClassBuilder<T> {

    /**
     * help to find field value by his name in a model
     *
     * @param object    the class model
     * @param fieldName the field name of the desired value
     * @return String value or null if not found
     */
    @Nullable
    public static String getString(@NotNull Object object, String fieldName) throws ClassNotFoundException {
        Class c = Class.forName(object.getClass().getName());
        for (Field field : c.getDeclaredFields()) {
            //skip if it is not the target field
            if (!field.getName().equals(fieldName)) continue;
            //make field accessible for reading
            if (field.getModifiers() == Modifier.PRIVATE) {
                field.setAccessible(true);
            }

            try {
                if (field.getType() == String.class)
                    return String.valueOf(field.get(object));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * help to find field value by his name in a model
     *
     * @param object    the class model
     * @return String value or null if not found
     */
    public static void fillAttribute(Object object, Cursor cursor) throws ClassNotFoundException {
        Class c = Class.forName(object.getClass().getName());
        for (Field field : c.getDeclaredFields()) {
            //make field accessible for reading
            if (field.getModifiers() == Modifier.PRIVATE) {
                field.setAccessible(true);
            }

            try {
                int index = cursor.getColumnIndex(field.getName());
                if (index == -1) continue;
                if (field.getType() == String.class)
                    field.set(object, cursor.getString(index));
                else if (field.getType() == int.class)
                    field.setInt(object, cursor.getInt(index));
                else if (field.getType() == boolean.class)
                    field.setBoolean(object, (cursor.getInt(index) != 0));
                else if (field.getType() == double.class)
                    field.setDouble(object, cursor.getDouble(index));
                else if (field.getType() == float.class)
                    field.setFloat(object, cursor.getFloat(index));
                else if (field.getType() == short.class)
                    field.setShort(object, cursor.getShort(index));
                else if (field.getType() == long.class)
                    field.setLong(object, cursor.getLong(index));
                // else if (field.getType() == byte.class)
                //field.setByte(object, cursor.getBlob(index).toString());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * help to find field value by his name in a model
     *
     * @param object    the class model
     * @param fieldName the field name of the desired value
     * @return int value or -1 if not found
     */
    @Nullable
    public static int getInt(@NotNull Object object, String fieldName) throws ClassNotFoundException {
        Class c = Class.forName(object.getClass().getName());
        for (Field field : c.getDeclaredFields()) {
            //skip if it is not the target field
            if (!field.getName().equals(fieldName)) continue;
            //make field accessible for reading
            if (field.getModifiers() == Modifier.PRIVATE) {
                field.setAccessible(true);
            }

            try {
                if (field.getType() == int.class)
                    return Integer.parseInt(String.valueOf(field.get(object)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }

    /**
     * @param object
     */
    public String printClass(T object) {
        try {
            Class c = Class.forName(object.getClass().getName());
            String result = "{";

            for (Field field : c.getDeclaredFields()) {
                field.setAccessible(true);
                try {

                    result = result.concat(String.format("\n\t\"%s\" : \"%s\",", field.getName(), field.get(object)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            return result.concat("}");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param object
     */
    public String printClassWithparent(T object) {
        try {
            Class c = Class.forName(object.getClass().getName());
            String result = "{";

            for (Field field : c.getDeclaredFields()) {
                field.setAccessible(true);
                try {

                    result = result.concat(String.format("\n\t\"%s\" : \"%s\",", field.getName(), field.get(object)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            for (Field field : c.getSuperclass().getDeclaredFields()) {
                if (field.getModifiers() == Modifier.PRIVATE) {
                    field.setAccessible(true);
                }
                try {
                    result = result.concat(String.format("\n\t\"%s\" : \"%s\",", field.getName(), field.get(object)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            return result.concat("}");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param json
     * @param object
     * @return
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public T buildClass(final ArrayList json, final T object) {
        int fieldIndex = 0;
        try {
            Class c = Class.forName(object.getClass().getName());

            for (Field field : c.getSuperclass().getDeclaredFields()) {
                if (field.getModifiers() == Modifier.PRIVATE) {
                    field.setAccessible(true);
                }
                field.set(object, json.get(fieldIndex));
                fieldIndex++;
            }
            fieldIndex = 0;
            for (Field field : c.getDeclaredFields()) {
                if (field.getModifiers() == Modifier.PRIVATE) {
                    field.setAccessible(true);
                }
                field.set(object, json.get(fieldIndex));
                fieldIndex++;
            }

        } catch (ClassNotFoundException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return object;
    }

    /**
     * @param object the model to extract data
     * @param values the object where extracted data from model will be put
     * @return an object with model data extracted
     */
    public ContentValues prepareStatement(final T object, ContentValues values) {
        try {
            Class c = Class.forName(object.getClass().getName());


            for (Field field : c.getDeclaredFields()) {
                if (!field.isAccessible()) field.setAccessible(true);
                try {
                    if (field.getName().equals("serialVersionUID")) continue;
                    if (field.getType() == String.class)
                        values.put(field.getName(), (String) field.get(object));
                    else if (field.getType() == int.class)
                        values.put(field.getName(), (Integer) field.get(object));
                    else if (field.getType() == boolean.class)
                        values.put(field.getName(), (Boolean) field.get(object));
                    else if (field.getType() == double.class)
                        values.put(field.getName(), (Double) field.get(object));
                    else if (field.getType() == float.class)
                        values.put(field.getName(), (Float) field.get(object));
                    else if (field.getType() == short.class)
                        values.put(field.getName(), (Short) field.get(object));
                    else if (field.getType() == long.class)
                        values.put(field.getName(), (Long) field.get(object));
                    else if (field.getType() == byte.class)
                        values.put(field.getName(), (Byte) field.get(object));

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return values;
    }
}
