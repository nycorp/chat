package corp.ny.com.chat.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import corp.ny.com.chat.R;
import corp.ny.com.chat.system.App;


/**
 * Created by Yann Yvan CEO of N.Y. Corp. on 20/03/18.
 */

public class DateManger {
    /**
     * Format date string to a simple date format
     *
     * @param date string to format
     * @return formatted string
     */
    public static String format(String date) {
        Calendar oldDate = Calendar.getInstance();
        oldDate.setTime(new Date(Timestamp.valueOf(date).getTime())); //set date on calendar
        if (Calendar.getInstance().get(Calendar.YEAR) - oldDate.get(Calendar.YEAR) != 0) {
            return dateStringFormatter(oldDate.getTime());
        }

        if (Calendar.getInstance().get(Calendar.MONTH) - oldDate.get(Calendar.MONTH) != 0) {
            return dateStringFormatter(oldDate.getTime());
        }

        int period = Calendar.getInstance().get(Calendar.DAY_OF_MONTH) - oldDate.get(Calendar.DAY_OF_MONTH);
        switch (period) {
            case 0:
                return App.getContext().getString(R.string.date_format_today);

            case 1:
                return App.getContext().getString(R.string.date_format_yesterday);
            default:
                return dateStringFormatter(oldDate.getTime());
        }
    }

    /**
     * Verify if the date is less than 2 days
     *
     * @param date as string
     * @return true if date is less than two days or false in other case
     */
    public static boolean isRecent(String date) {
        Calendar oldDate = Calendar.getInstance();
        oldDate.setTime(new Date(Timestamp.valueOf(date).getTime())); //set date on calendar
        if (Calendar.getInstance().get(Calendar.YEAR) - oldDate.get(Calendar.YEAR) != 0) {
            return false;
        }

        if (Calendar.getInstance().get(Calendar.MONTH) - oldDate.get(Calendar.MONTH) != 0) {
            return false;
        }
        int period = Calendar.getInstance().get(Calendar.DAY_OF_MONTH) - oldDate.get(Calendar.DAY_OF_MONTH);
        return period <= 1;
    }

    /**
     * Add a zero on value less than 10
     *
     * @param value to format
     * @return formatted string
     * <br/><b>Example</b><br/>input => output
     * <div>
     * 0 => 00|
     * 10 => 10|
     * 1 => 01
     * </div>
     */
    public static String dateZeroFormatter(int value) {
        if (value < 10) {
            return String.format("0%s", value);
        }
        return String.valueOf(value);
    }

    private static String dateStringFormatter(Date date) {
        return SimpleDateFormat.getDateInstance(DateFormat.FULL, Locale.ENGLISH).format(date);
    }
}
