package corp.ny.com.chat.views.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.chat.R;
import corp.ny.com.chat.model.User;
import corp.ny.com.chat.views.listener.OnItemClickListener;

public class ItemContactAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<User> users = new ArrayList<>();
    private OnItemClickListener<User> itemSelectedListener;

    private Context context;
    private LayoutInflater layoutInflater;
    private int lastId = -1;

    public ItemContactAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void addItem(User user) {
        if (user == null) return;
        for (User user1 : users) {
            if (user.getId() == user1.getId()) return;
        }
        users.add(user);
        notifyItemInserted(users.size());
    }

    public void addItems(List<User> users) {
        if (users.isEmpty()) {
            //lastId = users.get(0).getId();
            return;
        }
        for (User user : users) {
            if (user == null) continue;
            if (user.getId() < lastId) lastId = user.getId();
            this.users.add(user);
        }
        notifyDataSetChanged();
    }

    public void setItemSelectedListener(OnItemClickListener<User> itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_contact, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.name.setText(users.get(position).getName());


            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemSelectedListener != null)
                        itemSelectedListener.click(users.get(viewHolder.getAdapterPosition()));
                }
            });
        }
    }


    protected class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView name;
        private TextView date;
        private TextView lastMessage;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            date = view.findViewById(R.id.date);
            lastMessage = view.findViewById(R.id.last_message);
        }
    }
}
