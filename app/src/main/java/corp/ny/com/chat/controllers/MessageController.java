package corp.ny.com.chat.controllers;


import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import corp.ny.com.chat.env.Config;
import corp.ny.com.chat.env.Route;
import corp.ny.com.chat.model.Message;
import corp.ny.com.chat.response.DefaultResponse;
import corp.ny.com.chat.response.DefaultResponseAdapter;
import corp.ny.com.chat.utils.UnableToAccessInternetException;

/**
 * Created by yann-yvan on 15/01/18.
 */

public class MessageController extends Controller<Message> {
    DefaultResponse<Message> result;

    public MessageController(File cacheDir) {
        super(cacheDir);
    }

    @Override
    public DefaultResponse<Message> create(Message obj) throws IOException, JSONException, UnableToAccessInternetException {
        result = new DefaultResponse<>();

        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<Message> adapter = moshi.adapter(Message.class);
        final String json = adapter.toJson(obj);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", Config.getToken()).put("message", new JSONObject(json));
        System.out.println(jsonObject.toString(5));

        //execute our request here
        ArrayList data = super.makeRequest(Route.getPostMessage(), jsonObject.toString());
        System.out.println(data.get(1).toString());

        result.parseFromJson(data.get(1).toString(), new DefaultResponseAdapter() {
            @Override
            public void toDefaultWithModel(JSONObject jsonObject, DefaultResponse defaultResponse) throws JSONException, IOException {
                Moshi moshi = new Moshi.Builder().build();
                JsonAdapter<Message> adapter = moshi.adapter(Message.class);
                defaultResponse.setModel(adapter.fromJson(jsonObject.getString("bubble")));
            }
        });
        return result;
    }

    public DefaultResponse<Message> readNotification(List<Message> obj) throws JSONException, UnableToAccessInternetException {
        result = new DefaultResponse<>();

        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<Message> adapter = moshi.adapter(Message.class);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", Config.getToken());
        for (Message sms : obj) {
            jsonObject.accumulate("messages", new JSONObject(adapter.toJson(sms)));
        }
        System.out.println(jsonObject.toString(5));

        //execute our request here
        ArrayList data = super.makeRequest(Route.getReadNotification(), jsonObject.toString());
        result.parseFromJson(data.get(1).toString());
        return result;
    }


    @Override
    public DefaultResponse<Message> delete(Message obj) {
        return null;
    }

    @Override
    public DefaultResponse<Message> update(Message obj) {
        return null;
    }

    @Override
    public DefaultResponse<Message> find(String id) throws IOException, JSONException, UnableToAccessInternetException {
        result = new DefaultResponse<>();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", Config.getToken());
        System.out.println(jsonObject.toString(5));
        //execute our request here
        ArrayList data = super.makeRequest(Route.getGetContact(), jsonObject.toString());
        result.parseFromJsonToModelList(data.get(1).toString(), new DefaultResponseAdapter() {
            @Override
            public void toDefaultWithModel(JSONObject jsonObject, DefaultResponse defaultResponse) throws JSONException, IOException {
                Moshi moshi = new Moshi.Builder().build();
                JsonAdapter<Message> adapter = moshi.adapter(Message.class);
                //deserialize the json
                //deserialize each car found
                for (int i = 0; i < jsonObject.getJSONArray("users").length(); i++) {
                    JSONObject jsonCar = jsonObject.getJSONArray("users").getJSONObject(i);
                    Message user = adapter.fromJson(jsonCar.toString());
                    assert user != null;
                    //user.setProfilePicture(saveBase64Picture(String.valueOf(user.getId()), user.getProfilePicture(), FileDirectoryName.CAR));
                    defaultResponse.addModelToList(user);
                }

            }
        });
        return result;
    }

    public DefaultResponse<Message> updateDeviceID() throws JSONException, UnableToAccessInternetException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", Config.getToken()).put("fcmToken", Config.getDeviceID());
        System.out.print(jsonObject.toString(5));
        ArrayList data = super.makeRequest(Route.getUpdateDeviceId(), jsonObject.toString());
        //lets deserialize our response
        result = new DefaultResponse<>();
        result.parseFromJson(data.get(1).toString());
        return result;
    }
}
