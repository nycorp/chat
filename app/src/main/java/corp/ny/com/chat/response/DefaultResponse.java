package corp.ny.com.chat.response;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import corp.ny.com.chat.env.Config;

/**
 * Created by yann-yvan on 16/11/17.
 */

public class DefaultResponse<T> {
    private boolean status;
    private int message;
    private String token;
    private T model;
    private boolean hasNext = false;
    private ArrayList<T> modelList;

    public DefaultResponse() {
    }

    public boolean isStatus() {
        return status;
    }

    public int getMessage() {
        return message;
    }

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public String getToken() {
        return token;
    }

    public void parseFromJson(String response, DefaultResponseAdapter defaultResponseAdapter) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject(response);
        Log.i("server response", jsonObject.toString(5));
        int items = jsonObject.length();
        switch (items) {
            case 2:
                toDefault(jsonObject);
                break;
            case 3:
                toDefault(jsonObject);
                if (this.isStatus()) {
                    defaultResponseAdapter.toDefaultWithModel(jsonObject, this);
                } else {
                    parseFromJson(response);
                }
                break;
            case 4:
                toDefault(jsonObject);
                defaultResponseAdapter.toDefaultWithModel(jsonObject, this);
                this.token = jsonObject.getString("token");
                break;
            default:
                throw new JSONException("Wrong response format " + jsonObject.toString(6));
        }

    }

    /**
     * Parse result to default class without child class
     *
     * @param response the response receive from the serve that we want to parse to an object
     * @throws JSONException
     * @throws IOException
     */
    public void parseFromJson(String response) throws JSONException {
        Log.i("server response", response);
        JSONObject jsonObject = new JSONObject(response);
        int items = jsonObject.length();

        switch (items) {
            case 2:
                toDefault(jsonObject);
                break;
            case 3:
                this.token = jsonObject.getString("token");
                Config.setToken(this.token);
                break;
            default:
                throw new JSONException("Wrong response format " + jsonObject.toString(6));
        }
    }

    private void toDefault(JSONObject jsonObject) throws JSONException {
        this.status = jsonObject.getBoolean("status");
        this.message = jsonObject.getInt("message");
    }

    public void parseFromJsonToModelList(String response, DefaultResponseAdapter defaultResponseAdapter) throws JSONException, IOException {
        JSONObject jsonObject = new JSONObject(response);
        // Log.i("server response", jsonObject.toString(5));
        int items = jsonObject.length();
        switch (items) {
            case 2:
                toDefault(jsonObject);
                break;
            case 3:
                toDefault(jsonObject);
                if (this.isStatus()) {
                    defaultResponseAdapter.toDefaultWithModel(jsonObject, this);
                } else {
                    parseFromJson(response);
                }
                break;
            case 4:
                toDefault(jsonObject);
                defaultResponseAdapter.toDefaultWithModel(jsonObject, this);
                this.hasNext = jsonObject.getBoolean("has_next");
                break;
            default:
                throw new JSONException("Wrong response format " + jsonObject.toString(6));
        }
    }

    public void addModelToList(T model) {
        if (modelList == null) {
            modelList = new ArrayList<>();
        }
        modelList.add(model);
    }

    public ArrayList<T> getModelList() {
        return modelList;
    }

    public boolean hasNext() {
        return hasNext;
    }
}

