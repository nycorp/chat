package corp.ny.com.chat.env;

import android.support.annotation.NonNull;

/**
 * Created by yann-yvan on 03/11/17.
 */

public class Route {

    //server settings
    //private static final String BASE_URL = "http://nycorpserver.hopto.org/"; //local dynamic ip
    private static final String BASE_URL = "http://192.168.100.2:8081/"; //local static ip
    //private static final String BASE_URL = "http://192.168.1.120:8081/"; //local static ip
    private final static String LOGIN = "api/login";
    private final static String REGISTER = "api/mobile/auth/client/register";
    private final static String ACTIVATION = "api/mobile/auth/client/confirmation/code";
    private final static String RESEND_VERIFICATION_CODE = "api/mobile/auth/client/confirmation/resend";
    private final static String REGISTER_CAR = "api/mobile/client/car/register";
    private final static String UPDATE_CAR = "api/mobile/client/car/update";
    private final static String DELETE_CAR = "api/mobile/client/car/delete";
    private final static String GET_CONTACT = "api/user/find";
    private final static String POST_MESSAGE = "api/create/message";
    private final static String UPDATE_DEVICE_ID = "api/user/update/fcm";
    private final static String READ_NOTIFICATION = "api/message/read/notification";

    /**
     * Build complete route for any request
     *
     * @param route your target path on then server
     * @return full route with server URL
     */
    @NonNull
    public static String buildRoute(String route) {
        return BASE_URL.concat(route);
    }

    /**
     * Get activate account route
     *
     * @return the route string
     */
    public static String getActivation() {
        return ACTIVATION;
    }

    /**
     * Get resend verification code route
     *
     * @return the route string
     */
    public static String getResendVerificationCode() {
        return RESEND_VERIFICATION_CODE;
    }

    /**
     * Get login route
     *
     * @return the route string
     */
    public static String getLogin() {
        return LOGIN;
    }

    /**
     * Get register route
     *
     * @return the route string
     */
    public static String getRegister() {
        return REGISTER;
    }

    /**
     * Get register car route
     *
     * @return the route string
     */
    public static String getRegisterCar() {
        return REGISTER_CAR;
    }

    public static String getUpdateCar() {
        return UPDATE_CAR;
    }

    public static String getDeleteCar() {
        return DELETE_CAR;
    }

    public static String getGetContact() {
        return GET_CONTACT;
    }

    public static String getUpdateDeviceId() {
        return UPDATE_DEVICE_ID;
    }

    public static String getPostMessage() {
        return POST_MESSAGE;
    }

    public static String getReadNotification() {
        return READ_NOTIFICATION;
    }
}
