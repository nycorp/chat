package corp.ny.com.chat.controllers;


import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import corp.ny.com.chat.env.Config;
import corp.ny.com.chat.env.Route;
import corp.ny.com.chat.model.User;
import corp.ny.com.chat.response.DefaultResponse;
import corp.ny.com.chat.response.DefaultResponseAdapter;
import corp.ny.com.chat.utils.UnableToAccessInternetException;

/**
 * Created by yann-yvan on 15/01/18.
 */

public class UserController extends Controller<User> {
    DefaultResponse<User> result;

    public UserController(File cacheDir) {
        super(cacheDir);
    }

    public DefaultResponse<User> login(String username, String password) throws IOException, JSONException, UnableToAccessInternetException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("user", new JSONObject().put("email", username).put("password", password));

        //execute our request here
        ArrayList data = super.makeRequest(Route.getLogin(), jsonObject.toString());
        //lets deserialize our response
        result = new DefaultResponse<>();
        result.parseFromJson(data.get(1).toString(), new DefaultResponseAdapter() {
            @Override
            public void toDefaultWithModel(JSONObject jsonObject, DefaultResponse defaultResponse) throws JSONException, IOException {
                Moshi moshi = new Moshi.Builder().build();
                JsonAdapter<User> adapter = moshi.adapter(User.class);
                //deserialize the json
                defaultResponse.setModel(adapter.fromJson(jsonObject.getJSONObject("user").toString()));
            }
        });

        //return json deserialize response
        return result;
    }

    @Override
    public DefaultResponse<User> create(User obj) throws JSONException, UnableToAccessInternetException {
        result = new DefaultResponse<>();

        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<User> adapter = moshi.adapter(User.class);
        String json = adapter.toJson(obj);
        JSONObject jsonObject = new JSONObject(json);
        System.out.println(jsonObject.toString(5));

        //execute our request here
        ArrayList data = super.makeRequest(Route.getRegister(), json);
        result.parseFromJson(data.get(1).toString());
        return result;
    }

    /**
     * Activate user account
     *
     * @param email    user name, email or phone ...
     * @param password user password
     * @param code     activation code send to user by email or sms
     * @return @linkDefaultResponse object with data already parse to generic object
     * @throws JSONException fired when there is arn error in the response from the server
     * @throws IOException   fired when a network error occur
     */
    public DefaultResponse<User> activation(String email, String password, int code) throws JSONException, IOException, UnableToAccessInternetException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", code).put("user", new JSONObject().put("email", email).put("password", password));
        //execute our request here
        ArrayList data = super.makeRequest(Route.getActivation(), jsonObject.toString());
        //lets deserialize our response
        result = new DefaultResponse<>();
        result.parseFromJson(data.get(1).toString(), new DefaultResponseAdapter() {
            @Override
            public void toDefaultWithModel(JSONObject jsonObject, DefaultResponse defaultResponse) throws JSONException, IOException {
                Moshi moshi = new Moshi.Builder().build();
                //System.out.println(jsonObject.getJSONObject("user").toString(5));
                JsonAdapter<User> adapter = moshi.adapter(User.class);
                //deserialize the json
                defaultResponse.setModel(adapter.fromJson(jsonObject.getJSONObject("user").toString()));

            }
        });
        //Log.i("server response", data.get(0).toString());
        //return json deserialize response
        return result;
    }

    public DefaultResponse<User> resendCode(String email, String password) throws JSONException, UnableToAccessInternetException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("user", new JSONObject().put("email", email).put("password", password));
        System.err.print(jsonObject.toString());
        ArrayList data = super.makeRequest(Route.getResendVerificationCode(), jsonObject.toString());
        //lets deserialize our response
        result = new DefaultResponse<>();
        result.parseFromJson(data.get(1).toString());
        return result;
    }


    @Override
    public DefaultResponse<User> delete(User obj) {
        return null;
    }

    @Override
    public DefaultResponse<User> update(User obj) {
        return null;
    }

    @Override
    public DefaultResponse<User> find(String id) throws IOException, JSONException, UnableToAccessInternetException {
        result = new DefaultResponse<>();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", Config.getToken());
        //System.out.println(jsonObject.toString(5));
        //execute our request here
        ArrayList data = super.makeRequest(Route.getGetContact(), jsonObject.toString());
        result.parseFromJsonToModelList(data.get(1).toString(), new DefaultResponseAdapter() {
            @Override
            public void toDefaultWithModel(JSONObject jsonObject, DefaultResponse defaultResponse) throws JSONException, IOException {
                Moshi moshi = new Moshi.Builder().build();
                JsonAdapter<User> adapter = moshi.adapter(User.class);
                //deserialize the json
                //deserialize each car found
                for (int i = 0; i < jsonObject.getJSONArray("users").length(); i++) {
                    JSONObject jsonCar = jsonObject.getJSONArray("users").getJSONObject(i);
                    User user = adapter.fromJson(jsonCar.toString());
                    assert user != null;
                    //user.setProfilePicture(saveBase64Picture(String.valueOf(user.getId()), user.getProfilePicture(), FileDirectoryName.CAR));
                    defaultResponse.addModelToList(user);
                }

            }
        });
        return result;
    }

    public DefaultResponse<User> updateDeviceID() throws JSONException, UnableToAccessInternetException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("token", Config.getToken()).put("fcmToken", Config.getDeviceID());
        //System.out.print(jsonObject.toString(5));
        ArrayList data = super.makeRequest(Route.getUpdateDeviceId(), jsonObject.toString());
        //lets deserialize our response
        result = new DefaultResponse<>();
        result.parseFromJson(data.get(1).toString());
        return result;
    }
}
