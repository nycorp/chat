package corp.ny.com.chat.views.listener;

/**
 * Created by yann-yvan on 17/02/18.
 */

public interface OnItemClickListener<T> {
    void click(T model);
}
