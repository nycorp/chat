package corp.ny.com.chat.response;

import android.support.annotation.NonNull;

/**
 * Created by yann-yvan on 15/11/17.
 */

public final class RequestCode {

    /**
     * Call this to compare all authentication response code
     *
     * @param code rhe value you want to check
     * @return the corresponding enum
     */
    public static Auth authMessage(int code) {
        for (Auth auth :
                Auth.values()) {
            if (auth.equals(code)) {
                return auth;
            }
        }
        return Auth.UNKNOWN_CODE;
    }

    /**
     * Call this to compare all registration response code
     *
     * @param code rhe value you want to check
     * @return the corresponding enum
     */
    public static Registration registrationMessage(int code) {
        for (Registration registration :
                Registration.values()) {
            if (registration.equals(code)) {
                return registration;
            }
        }
        return Registration.UNKNOWN_CODE;
    }

    /**
     * Call this to compare all registration response code
     *
     * @param code rhe value you want to check
     * @return the corresponding enum
     */
    public static User memberMessage(int code) {
        for (User registration :
                User.values()) {
            if (registration.equals(code)) {
                return registration;
            }
        }
        return User.UNKNOWN_CODE;
    }

    /**
     * Call this to compare all verification response code
     *
     * @param code rhe value you want to check
     * @return the corresponding enum
     */
    public static Verification verificationMessage(int code) {
        for (Verification verification :
                Verification.values()) {
            if (verification.equals(code)) {
                return verification;
            }
        }
        return Verification.UNKNOWN_CODE;
    }

    /**
     * Call this to compare all token response code
     *
     * @param code rhe value you want to check
     * @return the corresponding enum
     */
    public static Token tokenMessage(int code) {
        for (Token token :
                Token.values()) {
            if (token.equals(code)) {
                return token;
            }
        }
        return Token.UNKNOWN_CODE;
    }

    public static DefResponse requestMessage(int code) {
        for (DefResponse response : DefResponse.values()
                ) {
            if (response.equals(code))
                return response;
        }
        return DefResponse.UNKNOWN_CODE;
    }

    /**
     * Authentication code
     */
    public enum Auth implements Required {
        LOGIN_SUCCESSFULLY(1000),
        WRONG_USERNAME(1001),
        WRONG_PASSWORD(1002),
        WRONG_USERNAME_OR_PASSWORD(1003),
        MISSING_CREDENTIALS_INFORMATION_REQUIRED(1004),
        UNKNOWN_CODE(0);

        private int code;

        Auth(int code) {
            this.code = code;
        }

        public int toCode() {
            return this.code;
        }

        public boolean equals(int code) {
            return this.code == code;
        }

        @Override
        public String toString() {
            return this.name();
        }

    }

    /**
     * Registration code
     */
    public enum Registration implements Required {
        ACCOUNT_CREATE_SUCCESSFULLY(1100),
        ACCOUNT_ALREADY_EXIST(1101),
        INVALID_EMAIL_ADDRESS(1102),
        UNABLE_SAVE_DATABASE(1103),
        UNKNOWN_CODE(0);

        private int code;

        Registration(int code) {
            this.code = code;
        }

        @Override
        public int toCode() {
            return this.code;
        }

        @Override
        public boolean equals(int code) {
            return this.code == code;
        }

        @Override
        public String toString() {
            return this.name();
        }
    }

    /**
     * /**
     * User code
     */
    public enum User implements Required {
        MISSING_REQUIRED_INFORMATION(1400),
        MEMBER_NOT_FOUND(1401),
        MEMBER_FOUND(1402),
        UNKNOWN_CODE(0);

        private int code;

        User(int code) {
            this.code = code;
        }

        @Override
        public int toCode() {
            return this.code;
        }

        @Override
        public boolean equals(int code) {
            return this.code == code;
        }

        @Override
        public String toString() {
            return this.name();
        }
    }

    /**
     * verification code
     */
    public enum Verification implements Required {
        ACCOUNT_ACTIVATED_SUCCESSFULLY(1200),
        ACCOUNT_NOT_ACTIVATE(1201),
        ACCOUNT_CONFIRMATION_CODE_EXPIRED_OR_DELETED(1202),
        ACCOUNT_CONFIRMATION_MISSING_INFORMATION(1203),
        ACCOUNT_CONFIRMATION_CODE_SEND_SUCCESSFULLY(1204),
        ACCOUNT_ALREADY_ACTIVE(1205),
        ACCOUNT_CONFIRMATION_CODE_SEND_FAILURE(1206),
        UNKNOWN_CODE(0);

        private int code;

        Verification(int code) {
            this.code = code;
        }

        public int toCode() {
            return this.code;
        }

        public boolean equals(int code) {
            return this.code == code;
        }

        @NonNull
        @Override
        public String toString() {
            return this.name();
        }
    }

    /**
     *
     */
    public enum DefResponse implements Required {
        SUCCESS(1500),
        FAILURE(1501),
        MISSING_INFORMATION(1502),
        UNKNOWN_CODE(0);
        private int code;

        DefResponse(int code) {
            this.code = code;
        }

        @Override
        public int toCode() {
            return this.code;
        }

        public boolean equals(int code) {
            return this.code == code;
        }

        @Override
        public String toString() {
            return this.name();
        }

    }

    /**
     * token code
     */
    public enum Token implements Required {

        TOKEN_EXPIRED(1),
        INVALID_TOKEN(2),
        NO_TOKEN(3),
        USER_NOT_FOUND(4),
        UNKNOWN_CODE(0);

        private int code;

        Token(int code) {
            this.code = code;
        }

        @Override
        public int toCode() {
            return this.code;
        }

        public boolean equals(int code) {
            return this.code == code;
        }

        @Override
        public String toString() {
            return this.name();
        }
    }

    private interface Required {
        int toCode();

        boolean equals(int code);

        String toString();
    }
}
