package corp.ny.com.chat.utils;

import android.util.Log;

/**
 * Created by yann-yvan on 03/01/18.
 */

public class UnableToAccessInternetException extends Exception {
    public UnableToAccessInternetException(String message) {
        super(message);
        Log.e("no internet", "Unable to reach server");
    }
}