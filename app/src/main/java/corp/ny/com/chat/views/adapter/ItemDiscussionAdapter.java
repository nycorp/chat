package corp.ny.com.chat.views.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import corp.ny.com.chat.R;
import corp.ny.com.chat.model.Message;
import corp.ny.com.chat.model.User;
import corp.ny.com.chat.views.listener.OnItemClickListener;

public class ItemDiscussionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<User> users = new ArrayList<>();
    private OnItemClickListener<User> itemSelectedListener;

    private Context context;
    private LayoutInflater layoutInflater;
    private int lastId = -1;

    public ItemDiscussionAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    public void addItem(User user) {
        for (User u : users) {
            if (u.getId() == user.getId()) {
                int index = users.indexOf(u);
                users.add(0, users.remove(index));
                notifyItemChanged(index);
                notifyItemMoved(index, 0);
                return;
            }
        }

        if (users.isEmpty()) {
            users.add(user);
            notifyItemInserted(users.size());
        } else {
            users.add(0, user);
            notifyItemInserted(0);
        }
    }

    public void addItems(List<User> users) {
        if (users.isEmpty()) {
            //lastId = users.get(0).getId();
            return;
        }
        for (User user : users) {
            if (user.getId() < lastId) lastId = user.getId();
            this.users.add(user);
        }
        notifyDataSetChanged();
    }

    public void setItemSelectedListener(OnItemClickListener<User> itemSelectedListener) {
        this.itemSelectedListener = itemSelectedListener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_discussion, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            User user = users.get(position);
            viewHolder.name.setText(user.getName());
            Message message = Message.getInstance().lastDiscussionMessage(user.getId());
            viewHolder.setLastMessage(message.getText());
            viewHolder.setUnread(message.countUnreadMessage(user.getId()));
            viewHolder.setDate(message.getDate());

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemSelectedListener != null)
                        itemSelectedListener.click(users.get(viewHolder.getAdapterPosition()));
                }
            });

        }
    }


    protected class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView name;
        private TextView date;
        private TextView lastMessage;
        private TextView unread;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            name = view.findViewById(R.id.name);
            date = view.findViewById(R.id.date);
            lastMessage = view.findViewById(R.id.last_message);
            unread = view.findViewById(R.id.unread);
        }

        public void setLastMessage(String message) {
            lastMessage.setVisibility(View.VISIBLE);
            lastMessage.setText(message);
        }

        public void setUnread(int count) {
            if (count > 0) {
                unread.setVisibility(View.VISIBLE);
                unread.setText(String.valueOf(count));
                date.setTextColor(context.getResources().getColor(R.color.accent));
            }
        }

        public void setDate(String time) {
            if (time != null) {
                date.setVisibility(View.VISIBLE);
                date.setText(time.substring(11, 16));
            }
        }
    }
}
