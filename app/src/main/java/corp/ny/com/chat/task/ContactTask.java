package corp.ny.com.chat.task;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.IOException;

import corp.ny.com.chat.controllers.UserController;
import corp.ny.com.chat.model.User;
import corp.ny.com.chat.response.DefaultResponse;
import corp.ny.com.chat.utils.UnableToAccessInternetException;
import corp.ny.com.chat.views.listener.OnItemClickListener;

import static corp.ny.com.chat.response.RequestCode.requestMessage;
import static corp.ny.com.chat.response.RequestCode.tokenMessage;


/**
 * Created by yann-yvan on 03/01/18.
 */

public class ContactTask extends AsyncTask<Void, Void, DefaultResponse<User>> {

    private Context context;
    private OnItemClickListener<User> listener;


    public ContactTask(Context context) {
        this.context = context;
    }

    public void setListener(OnItemClickListener<User> listener) {
        this.listener = listener;
    }

    @Override
    protected DefaultResponse<User> doInBackground(Void... params) {
        UserController controller = new UserController(context.getCacheDir());

        try {
            return controller.find("");
        } catch (IOException | JSONException | UnableToAccessInternetException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(DefaultResponse<User> clientDefaultResponse) {
        //check is the request has not failed or bad server response
        if (clientDefaultResponse == null) {
            //Log.e("bad request", "something when wrong");
            return;
        }

        switch (requestMessage(clientDefaultResponse.getMessage())) {
            case SUCCESS:
                Log.e("Update", "success");
                for (User user :
                        clientDefaultResponse.getModelList()) {
                    if (listener != null) {
                        listener.click(user.save());
                    }
                }
                break;

            case FAILURE:
                Log.e("Update", "fails");
                break;

            case UNKNOWN_CODE:

                switch (tokenMessage(clientDefaultResponse.getMessage())) {
                    case NO_TOKEN:

                        break;
                    case INVALID_TOKEN:

                        break;
                    case TOKEN_EXPIRED:

                        break;
                    case USER_NOT_FOUND:

                        break;
                    case UNKNOWN_CODE:

                        break;
                }
                break;
        }

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
